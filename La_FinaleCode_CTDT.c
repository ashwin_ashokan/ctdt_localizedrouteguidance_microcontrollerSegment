#include <IRremote.h>
#include<LedControl.h>
LedControl lc = LedControl(10,8,9,2);//Shift Register Object (dataPin,clockPin,csPin,numDevices)
#define allLitUp 46




const int RECEIVE_PIN = 3;
IRrecv remoteReceive(RECEIVE_PIN);
decode_results valueReceived;
int ones;
int tens;
int counter;
int remoteSingleInput();
int mapLocationIndex();
void outputBinaryValue(int);
void portF_Initialization();
void portK_Initialization();
void portA_Initialization();
void portC_Initialization();
void portL_Initialization();
void portWriting(int[5][8]);
void ledDisplay(int,int);
void displayInitialization();
void clearMapDisplay();
void disco(int);


byte ledData[10][8]={

  {
B00111100,                
B01000010,
B01000010,
B01000010,    
B01000010,
B01000010,
B01000010,
B00111100,
},

{
B00111000,
B01011000,
B10011000,
B00011000,
B00011000,
B00011000,
B00011000,
B11111111,
},

{
B00111110,
B01000001,
B01000001,
B00000010,
B00001100,
B00011000,
B00110000,
B01111111,
},

{
B01111111,
B00000010,
B00000100,
B00001000,
B00000110,
B00000001,
B10000010,
B01111100,
},

{
B00001000,
B00010000,
B00100000,
B01000100,
B11111111,
B00000100,
B00000100,
B00000100,
},

{
B01111111,
B01000000,
B01000000,
B01111110,
B00000001,
B00000001,
B00000001,
B01111110,
},

{
B00111111,
B01000000,
B01000000,
B01000000,
B01111110,
B01000001,
B01000001,
B00111111,
},

{
B01111111,
B00000011,
B00000110,
B00001100,
B00011000,
B00011000,
B00011000,
B00011000,
},


{
B01111110,
B01000010,
B01000010,
B01000010,
B01111110,
B01000010,
B01000010,
B01111110,
},

{
B00111110,
B01000001,
B01000001,
B01000001,
B00111111,
B00000001,
B00000001,
B00111110,
}

};
  
int bin[5][8];
int coordinates[47][5] = {
  {0,0,0,0,0},//Testing column
  {0,136,24,0,64},//1
  {0,140,24,0,64},//2
  {0, 200, 24, 0,72},//3
  {0, 200, 24, 0, 73},//4
  {0,200, 24, 0, 64},//5
  {0, 172,24, 0,64},//6
  {0,156,24, 0,64},//7
  {0,200,24, 0,66},//8
  {0, 200,24, 0,66},//9
  {0,200,24, 0,64},//10
  {0,200,24, 0,73},//11
  {0,0,16, 0,208},//12
  {0,0,16, 0,192},//13
  {0,0,16, 0,64},//14
  {0,0,16,0,192},//15
  {0,0,16,0,192},//16
  {0,0,0,0,32},//17
  {16,0,0,0,32},//18
  {0,0,0,0,32},//19
  {17,0,0,0,32},//20
  {17,0,0,0,32},//21
  {17,0,0,0,32},//22
  {0,128,16,0,64},//23
  {0,128,16,0,64},//24
  {0,128,24,132,64},//25
  {0,128,24,132,64},//26
  {0,141,24,0,64},//27
  {0,128,24,132,64},//28
  {0,128,24,18,64},//29
  {0,128,24,18,64},//30
  {128,0,0,0,32},//31
  {128,0,0,0,32},//32
  {17,0,0,0,32},//33
  {17,0,0,0,32},//34
  {23,0,32,0,32},//35
  {21,0,0,0,32},//36
  {0,130,24,128,64},//37
  {0,130,24,128,64},//38
  {0,130,24,128,64},//39
  {0,130,24,128,64},//40
  {0,130,24,128,64},//41
  {0,0,1,0,64},//42
  {8,0,1,0,64},//43
  {8,0,1,0,64},//44
  {23,0,2,0,32},//45 Over
  {255,255,255,255,255}//46
};

void setup()
{

  for(int i =0;i<1;++i) lc.setIntensity(i,15); //setting brightness intensity for LED lights
  for(int i =0;i<1;++i) lc.clearDisplay(i);
  
  Serial.begin(9600);
  remoteReceive.enableIRIn();
  remoteReceive.blink13(true);
  portF_Initialization();
  portK_Initialization();
  portA_Initialization();
  portC_Initialization();
  portL_Initialization();
 displayInitialization();
 outputBinaryValue(allLitUp);//Used for outputting the values
 portWriting(bin);//Writing it to the port values
 delay(300);
 clearMapDisplay();
 }



void loop()
{
  outputBinaryValue(mapLocationIndex());
  portWriting(bin);
}

void disco(int a)
{
for(int i = 1;i<=46;++i)
  {
    outputBinaryValue(i);
  portWriting(bin);
  ledDisplay(i/10,2);
  ledDisplay(i%10,1);
  delay(a);
  }
}

void clearMapDisplay()
{
  outputBinaryValue(0);
  portWriting(bin);
}

void displayInitialization()
{
  lc.shutdown(0,false);  // Wake up displays
  lc.shutdown(1,false);
  lc.setIntensity(0,2);  // Set intensity levels
  lc.setIntensity(1,2);
  lc.clearDisplay(0);  // Clear Displays
  lc.clearDisplay(1);
}


  
  void ledDisplay(int value,int displayNumber)
  {
  for(int i=0;i<=7;++i)
  {
    lc.setRow(displayNumber-1,i,ledData[value][i]);  //(DisplayNumber,row_no,Byte of Data)
    }
    }



void outputBinaryValue(int index)
{
  //It must take in the index value
  //Correlate the index value to corresponding Matrix Row
  //Convert the corresponding matrix[indexValue][5] to binary values
  //Save the binary values to binArray[5][8]
  //Use each binArray row to change state of the pinValues(Independent function

  for (int j = 1; j <= 5; ++j) //looping over the 5 columns
  {int temp = coordinates[index][j - 1];
    for (int i = 1; i <= 8; ++i) //looping over individual bits
    {
       bin[j - 1][i - 1] = (temp) % 2;
      temp/=2;
      
    }
   
  }


  

for (int j = 1; j <= 5; ++j) //looping over the 5 columns for diplaying the values once pressed
  {
    for (int i = 1; i <= 8; ++i) //looping over individual bits
    {
      Serial.print(bin[j - 1][7 - (i - 1)]);
    }
    Serial.println(" ");
  }
}


int mapLocationIndex() // used for combining two consecutively entered Numerical Values from the Remote
{
int tens = remoteSingleInput();
lc.clearDisplay(0);  // Clear Displays
lc.clearDisplay(1);
ledDisplay(tens,2);
int ones = remoteSingleInput();
ledDisplay(ones,1);
clearMapDisplay();
   int dummy = tens * 10 + ones;
  if((dummy>46)&& (dummy!=99)&&(dummy!=98)) return 0;
  else if(dummy == 99) 
  {
    disco(250);
    return 0;
  }
  else if(dummy==98)
  {
    disco(2);
    return 0;
  }
 else
{
  Serial.println(dummy);
  return dummy;
}
}


void portWriting(int matrix[5][8])
{

  digitalWrite(A0, matrix[0][0]);//A0-A7 port 1(Human Convention)
  digitalWrite(A1, matrix[0][1]);
  digitalWrite(A2, matrix[0][2]);
  digitalWrite(A3, matrix[0][3]);
  digitalWrite(A4, matrix[0][4]);
  digitalWrite(A5, matrix[0][5]);
  digitalWrite(A6, matrix[0][6]);
  digitalWrite(A7, matrix[0][7]);//A0-A7 port 1(Human Convention)
  digitalWrite(A8, matrix[1][0]);//A8 -A15 port 4(Human Convention)
  digitalWrite(A9, matrix[1][1]);
  digitalWrite(A10, matrix[1][2]);
  digitalWrite(A11, matrix[1][3]);
  digitalWrite(A12, matrix[1][4]);
  digitalWrite(A13, matrix[1][5]);
  digitalWrite(A14, matrix[1][6]);
  digitalWrite(A15, matrix[1][7]);//A8 -A15 port 4(Human Convention)

for(byte i = 0;i<=7;++i)  digitalWrite((22+(i*2)), matrix[4][i]);//Port 5 port (Human Convention)
for(byte i = 0;i<=7;++i)  digitalWrite((39+(i*2)), matrix[3][i]);//Port 4 port (Human Convention)
for(byte i = 0;i<=7;++i)  digitalWrite(14+i, matrix[2][i]);//Port 3 port (Human Convention)
}







void portF_Initialization()
{
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A5, OUTPUT);
  pinMode(A6, OUTPUT);
  pinMode(A7, OUTPUT);
}



void portK_Initialization()
{
  pinMode(A8, OUTPUT);
  pinMode(A9, OUTPUT);
  pinMode(A10, OUTPUT);
  pinMode(A11, OUTPUT);
  pinMode(A12, OUTPUT);
  pinMode(A13, OUTPUT);
  pinMode(A14, OUTPUT);
  pinMode(A15, OUTPUT);
}



void portA_Initialization()
{
  for(byte i = 22;i<=36;++(++i))  pinMode(i, OUTPUT);
}

void portC_Initialization()
{
  for(byte i = 23;i<=37;++(++i)) pinMode(i, OUTPUT);
  }

void portL_Initialization()
{
  for(byte i =14;i<=21;++i) pinMode(i, OUTPUT);
}


int remoteSingleInput() //For obtaining single value from the Remote Control
{
  int count = 0;
  int ones;

  while (count == 0)
  {
    if (remoteReceive.decode(&valueReceived) && count == 0)
    {
      Serial.println(valueReceived.value);
      if (valueReceived.value == 0XFFFFFFFF)//Prevention of Recursive Buttons
      {
        valueReceived.value = 0;
      }
      else
      {
        ++count;//Indicate a different Numerical is Pressed
        switch (valueReceived.value) //Check for the number pressed
        {
          case 0x1FEC03F: Serial.println(" FAST FORWARD");
            break;
          case 0x1FE40BF: Serial.println(" REWIND");
            break;
          case 0x1FE58A7: Serial.println("MODE");
            break;
          case 0x1FE7887: Serial.println("MUTE");
            break;
          case 0x1FE609F: Serial.println(" VOLUME UP");
            break;
          case 0x1FEA05F: Serial.println("VOLUME DOWN");
            break;
          case 0x1FE50AF: ones = 1;
            break;
          case 0x1FED827: ones =  2;
            break;
          case 0x1FEF807: ones = 3;
            break;
          case 0x1FE30CF: ones = 4;
            break;
          case 0x1FEB04F: ones = 5;
            break;
          case 0x1FE708F: ones = 6;
            break;
          case 0x1FE00FF: ones = 7;
            break;
          case 0x1FEF00F: ones = 8;
            break;
          case 0x1FE9867: ones = 9;
            break;
          case 0x1FE10EF: Serial.println("REPEAT");
            break;
          case 0x1FEE01F: ones = 0;
            break;
          case 0xF0C41643: Serial.println("EQUALIZER BEATS");
            break;
          default: count = 0;
            Serial.println("INSUFFICIENT INFORMATION");
            break;
        }
      }
      remoteReceive.resume();//Go to receive next digit otherwise it will stay in the same digit
    }
  }

  if (count == 1)
  {
    Serial.println(ones);//Printing the temporary value
    delay(150);// Delay to prevent debouncing.
  }
  return ones;

}
